FROM nginx

COPY . /usr/share/nginx/html

WORKDIR /home/project

COPY . .

EXPOSE 8289